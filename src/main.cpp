//Using SDL and standard IO
#include <SDL.h>
#include <memory>
#include <SDL_keyboard.h>
#include <chrono>
#include <iostream>

#define main SDL_main

//Screen dimension constants
const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 720;

struct sdl_window_deleter {
    void operator()(SDL_Window *window) const {
        SDL_DestroyWindow(window);
    }
};

struct sdl_surface_deleter {
    void operator()(SDL_Surface *surface) const {
        SDL_FreeSurface(surface);
    }
};

int main(int argv, char** args) {
    //The window we'll be rendering to
    std::unique_ptr<SDL_Window, sdl_window_deleter> window(nullptr);

    //The surface contained by the window
    std::unique_ptr<SDL_Surface, sdl_surface_deleter> screenSurface(nullptr);

    //Initialize SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
    } else {
        //Create window
        window.reset(SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH,
                                      SCREEN_HEIGHT, SDL_WINDOW_SHOWN));

        if (window == nullptr) {
            printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
        } else {
            //Get window surface
            screenSurface.reset(SDL_GetWindowSurface(window.get()));

            bool quit = false;
            SDL_Event e;

            float playerY = 100;
            SDL_Rect playerRect = SDL_Rect{10, (int) playerY, 25, 125};
            bool moveUp = false;
            bool moveDown = false;

            auto previousFrameStart = std::chrono::high_resolution_clock::now();

            Uint32 frameDelay = 10;

            // Main game loop
            while (!quit) {
                auto frameStart = std::chrono::high_resolution_clock::now();
                std::chrono::duration<float> frameDuration = frameStart - previousFrameStart;
                float deltaTime = std::chrono::duration_cast<std::chrono::duration<float>>(frameDuration).count();
                previousFrameStart = frameStart;


                while (SDL_PollEvent(&e) != 0) {
                    switch (e.type) {
                        case SDL_KEYDOWN:
                            switch (e.key.keysym.sym) {
                                case SDLK_UP:
                                    moveUp = true;
                                    break;
                                case SDLK_DOWN:
                                    moveDown = true;
                                    break;
                                case SDLK_p:
                                    frameDelay += 1;
                                    printf("Setting frame delay to %d\n", frameDelay);
                                    break;
                                case SDLK_o:
                                    frameDelay -= 1;
                                    printf("Setting frame delay to %d\n", frameDelay);
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case SDL_KEYUP:
                            switch (e.key.keysym.sym) {
                                case SDLK_w:
                                case SDLK_UP:
                                    moveUp = false;
                                    break;
                                case SDLK_s:
                                case SDLK_DOWN:
                                    moveDown = false;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case SDL_QUIT:
                            quit = true;
                            break;
                        default:
                            break;
                    }
                }

                //Fill the surface black
                SDL_FillRect(screenSurface.get(), nullptr, SDL_MapRGB(screenSurface->format, 0x00, 0x00, 0x00));

                // Move the player
                if (moveUp) {
                    playerY -= 500 * deltaTime;
                    if (playerY <= 0) {
                        playerY = 0;
                    }
                } else if (moveDown) {
                    playerY += 500 * deltaTime;
                    if (playerY >= SCREEN_HEIGHT - 125) {
                        playerY = SCREEN_HEIGHT - 125;
                    }
                }

                playerRect.y = (int) playerY;

                // Draw the player
                SDL_FillRect(screenSurface.get(), &playerRect, SDL_MapRGB(screenSurface->format, 0xFF, 0xFF, 0xFF));

                //Update the surface
                SDL_UpdateWindowSurface(window.get());

                std::flush(std::cout);

                SDL_Delay(frameDelay);
            }
        }
    }

    // Delete the surface
    screenSurface.reset(nullptr);

    //Quit SDL subsystems
    SDL_Quit();

    return EXIT_SUCCESS;
}
